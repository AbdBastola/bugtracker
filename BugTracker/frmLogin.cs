﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugTracker
{
    /// <summary>
    /// this class contains methods used in this form
    /// </summary>
    public partial class frmLogin : Form
    {

        /// <summary>
        /// this is the constructor of this class
        /// </summary>
        public frmLogin()
        {
            InitializeComponent();
        }

        private void btnregister_Click(object sender, EventArgs e)
        {
            
        }


        /// <summary>
        /// this method is called when login button is clicked
        /// this method enables to retrieve data from database according to the text value provided
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnlogin_Click(object sender, EventArgs e)
        {
            //checking whether the text is empty and applying actions according to it
            if (txtUser.Text == "" || txtPass.Text == "")
            {
                MessageBox.Show("Required Fields must not be empty");
            }
            else 
            {
                //establishing connection to the database and opening connection
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
                con.Open();

                //creating string variable to store sql sataement
                String str = "Select first_name,role From Users where username = '" + txtUser.Text + "' and password = '" + txtPass.Text + "' ";

                //creating sql command object to execute the query
                SqlCommand cmd = new SqlCommand(str, con);

                //creating data apdater obj and fetching the data into the datatable
                SqlDataAdapter sda = new SqlDataAdapter(str,con);
                DataTable dt = new DataTable();
                sda.Fill(dt);
                if (dt.Rows.Count > 0)
                {

                    MessageBox.Show("Login Successfull !!");
                    this.Hide();

                    //if the role is manager redirect to manager dashboard
                    if (dt.Rows[0][1].ToString() == "Manager")
                    {
                        Dashboard das = new Dashboard(dt.Rows[0][1].ToString(), dt.Rows[0][0].ToString());
                        das.Show();
                    }
                    //if the role is Developer redirect to Developer dashboard
                    else if (dt.Rows[0][1].ToString() == "Developer")
                    {
                        DashboardDeveloper dev = new DashboardDeveloper(dt.Rows[0][1].ToString(), dt.Rows[0][0].ToString());
                        dev.Show();
                    }
                    //if the role is Tester redirect to Tester dashboard
                    else if (dt.Rows[0][1].ToString() == "Tester")
                    {
                        DashboardDeveloper dev = new DashboardDeveloper(dt.Rows[0][1].ToString(), dt.Rows[0][0].ToString());
                        dev.Show();
                    }
                   
                    
                   //sql data reader object and executing reafer
                   SqlDataReader dr;
                    dr = cmd.ExecuteReader();


                }
                else
                {
                    MessageBox.Show("Plese Check the login credentials");
                }
            }
        }

        /// <summary>
        /// this method is called when the link label is called
        /// ths merhod closes this form and open the register form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //hiding this form 
            this.Hide();

            //creating object of another form
            frmRegister reg = new frmRegister();

            //show another form
            reg.Show();
        }
    }
}
