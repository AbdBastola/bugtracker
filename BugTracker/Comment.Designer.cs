﻿namespace BugTracker
{
    partial class Comment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmbBugs = new System.Windows.Forms.ComboBox();
            this.bugsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bugsDataSet1 = new BugTracker.bugsDataSet1();
            this.label2 = new System.Windows.Forms.Label();
            this.bugsTableAdapter = new BugTracker.bugsDataSet1TableAdapters.BugsTableAdapter();
            this.listComments = new System.Windows.Forms.ListBox();
            this.textComment = new System.Windows.Forms.TextBox();
            this.buttonComment = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsDataSet1)).BeginInit();
            this.SuspendLayout();
            // 
            // cmbBugs
            // 
            this.cmbBugs.DataSource = this.bugsBindingSource;
            this.cmbBugs.DisplayMember = "bug_title";
            this.cmbBugs.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbBugs.FormattingEnabled = true;
            this.cmbBugs.Location = new System.Drawing.Point(5, 28);
            this.cmbBugs.Name = "cmbBugs";
            this.cmbBugs.Size = new System.Drawing.Size(414, 24);
            this.cmbBugs.TabIndex = 1;
            this.cmbBugs.ValueMember = "Id";
            this.cmbBugs.SelectedValueChanged += new System.EventHandler(this.comboBox1_SelectedValueChanged);
            // 
            // bugsBindingSource
            // 
            this.bugsBindingSource.DataMember = "Bugs";
            this.bugsBindingSource.DataSource = this.bugsDataSet1;
            // 
            // bugsDataSet1
            // 
            this.bugsDataSet1.DataSetName = "bugsDataSet1";
            this.bugsDataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(2, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(132, 16);
            this.label2.TabIndex = 2;
            this.label2.Text = "Select Bugs";
            // 
            // bugsTableAdapter
            // 
            this.bugsTableAdapter.ClearBeforeFill = true;
            // 
            // listComments
            // 
            this.listComments.CausesValidation = false;
            this.listComments.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.listComments.FormattingEnabled = true;
            this.listComments.HorizontalExtent = 5;
            this.listComments.HorizontalScrollbar = true;
            this.listComments.ItemHeight = 16;
            this.listComments.Location = new System.Drawing.Point(5, 59);
            this.listComments.Name = "listComments";
            this.listComments.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.listComments.Size = new System.Drawing.Size(453, 340);
            this.listComments.Sorted = true;
            this.listComments.TabIndex = 3;
            // 
            // textComment
            // 
            this.textComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textComment.Location = new System.Drawing.Point(1, 416);
            this.textComment.Multiline = true;
            this.textComment.Name = "textComment";
            this.textComment.Size = new System.Drawing.Size(320, 47);
            this.textComment.TabIndex = 4;
            // 
            // buttonComment
            // 
            this.buttonComment.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.buttonComment.Location = new System.Drawing.Point(327, 433);
            this.buttonComment.Name = "buttonComment";
            this.buttonComment.Size = new System.Drawing.Size(92, 29);
            this.buttonComment.TabIndex = 5;
            this.buttonComment.Text = "Comment";
            this.buttonComment.UseVisualStyleBackColor = true;
            this.buttonComment.Click += new System.EventHandler(this.buttonComment_Click);
            // 
            // Comment
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(470, 466);
            this.Controls.Add(this.buttonComment);
            this.Controls.Add(this.textComment);
            this.Controls.Add(this.listComments);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.cmbBugs);
            this.Name = "Comment";
            this.Text = "Comment";
            this.Load += new System.EventHandler(this.Comment_Load);
            ((System.ComponentModel.ISupportInitialize)(this.bugsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsDataSet1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.ComboBox cmbBugs;
        private System.Windows.Forms.Label label2;
        private bugsDataSet1 bugsDataSet1;
        private System.Windows.Forms.BindingSource bugsBindingSource;
        private bugsDataSet1TableAdapters.BugsTableAdapter bugsTableAdapter;
        private System.Windows.Forms.ListBox listComments;
        private System.Windows.Forms.TextBox textComment;
        private System.Windows.Forms.Button buttonComment;
    }
}