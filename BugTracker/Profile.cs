﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.IO;

namespace BugTracker
{
    public partial class Profile : Form
    {
        string Imagepath = "";
        int Id = 0;
        public Profile(String name)
        {
            
           //intializing the component when the form is loaded
            InitializeComponent();
          
            //connection to the database and opening connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //string variable to store the query
            String str = "Select * From Users where first_name = '" + name + "' ";

            //command to prepare the query
            SqlCommand cmd = new SqlCommand(str, con);

            //dataAdapter to prepare the query and fill the data to data table
            SqlDataAdapter sda = new SqlDataAdapter(str, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            
            //if rows count is greater than zero
            if (dt.Rows.Count > 0)
            {
                //filling the text accrding to the rows fetched in database
                txtfname.Text = dt.Rows[0][3].ToString();
                txtlname.Text = dt.Rows[0][4].ToString();
                txtadd.Text = dt.Rows[0][6].ToString();
               txtemail.Text = dt.Rows[0][5].ToString();
                txtcont.Text = dt.Rows[0][7].ToString();
                txtuname.Text = dt.Rows[0][1].ToString();
                txtpass.Text = dt.Rows[0][2].ToString();
                cmbrole.Text = dt.Rows[0][8].ToString();
                Id = Convert.ToInt32(dt.Rows[0][0].ToString());
                
               // SqlDataReader dr;
                //dr = cmd.ExecuteReader();
              
            }

            //creating a variable to store sql query
            String strt = "Select image From image where user_id = '" + Id + "' ";

            //command to prepare the statement
            SqlCommand cmde = new SqlCommand(strt, con);

            //data reader to read the data
            SqlDataReader sde = cmde.ExecuteReader();
            sde.Read();

            if (sde.HasRows)
            {
                //creating an byte array to store bytes
                byte[] img = ((byte[])sde[0]);

                //if byte img is null
                if(img == null)
                {

                    //assinging picture box as null
                    picImage.Image = null;

                    
                }
                else
                {
                    //creating a memorystream and wriring it in picture box
                    MemoryStream st = new MemoryStream(img);
                    picImage.Image = Image.FromStream(st);
                }
            }

            //if picture box is null
            if (picImage.Image == null)
            {

                //button delete is not visible else
                btnDelete.Visible = false;
            }
            else
            {
                //button Delete is visible
                btnDelete.Visible = true;
            }

        }

        private void bUpload_Click(object sender, EventArgs e)
        {
         
            //creating opefiledialog object
            OpenFileDialog openFileDialog = new OpenFileDialog();
           
            //filteing the dialog
            openFileDialog.Filter = "PNG Files (*.png)|*.png|JPEG Files (*.jpg*)|*.jpg*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                //assinging image path and imagelocation
                Imagepath = openFileDialog.FileName.ToString();
                picImage.ImageLocation = Imagepath;

                //array byte is null
                byte[] images = null;

                //creating object of filestraem and assng the onject to binary reader
                FileStream file = new FileStream(Imagepath, FileMode.Open, FileAccess.Read);
                BinaryReader br = new BinaryReader(file);

                //reading bytes of the image
                images = br.ReadBytes((int)file.Length);

                //creating connection to the database and opening it
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
                con.Open();

                //creating variable to store sql qery
                String str = "Insert into image(image,user_id)Values(@images,@id) ";

                //creating sql command object and adding parameters to the query
                SqlCommand cmd = new SqlCommand(str, con);
                cmd.Parameters.Add(new SqlParameter("@images",images));
                cmd.Parameters.Add(new SqlParameter("@id", Id));

                //execting the query and closing the connection
                cmd.ExecuteNonQuery();
                con.Close();
                MessageBox.Show("Picture Uploaded");
               
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            //checking whether the text is empty or not
            if (txtuname.Text == "" || txtpass.Text == "")
            {
                MessageBox.Show("Required Fields must not be empty");
            }
            else if (txtpass.Text == txtcpass.Text)
            {
                //creating connection to the database and opening it
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
                con.Open();

                //creating sql command object and assing command type as stored procedure
                SqlCommand cmd = new SqlCommand("UserUpdate", con);
                cmd.CommandType = CommandType.StoredProcedure;

                //adding parameters to the stored procedure and it's value
                cmd.Parameters.AddWithValue("@username", txtuname.Text.Trim());
                cmd.Parameters.AddWithValue("@password", txtpass.Text.Trim());
                cmd.Parameters.AddWithValue("@first_name", txtfname.Text.Trim());
                cmd.Parameters.AddWithValue("@last_name", txtlname.Text.Trim());
                cmd.Parameters.AddWithValue("@email", txtemail.Text.Trim());
                cmd.Parameters.AddWithValue("@address", txtadd.Text.Trim());
                cmd.Parameters.AddWithValue("@number", txtcont.Text.Trim());
                cmd.Parameters.AddWithValue("@role", cmbrole.Text.Trim());

                //executing the command
                cmd.ExecuteNonQuery();
                MessageBox.Show("Profile Updated Successfully !!");
                //clear();

              


            }
            else
            {
                label13.Show();
                label13.Text = "Password Mismatch";
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            //connecting to the database and opening the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //a variable to store the sql query
            String str = "Delete from image where user_id = '" + Id + "' ";

            //sql command to prepare the query to the database
            SqlCommand cmd = new SqlCommand(str, con);

            //executing the query
            cmd.ExecuteNonQuery();
           
            //closing the connection
            con.Close();
            MessageBox.Show("Picture Deleted");
           
        }
    }
    }

