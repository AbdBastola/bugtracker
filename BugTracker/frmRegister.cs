﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
namespace BugTracker
{

    /// <summary>
    /// this class contains methods used in this form
    /// </summary>
    public partial class frmRegister : Form
    {
        /// <summary>
        /// this is the constructor of the class frmRegister
        /// </summary>
        public frmRegister()
        {
            InitializeComponent();
        }

        private void btn_login_Click(object sender, EventArgs e)
        {
            
        }

        /// <summary>
        /// this methood is called when button register is clicked
        /// this method enables to add data into the database using stored procedure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_register_Click(object sender, EventArgs e)
        {
            //checking whether the text is empty and applying actions according to it
            if (txtuname.Text == "" || txtpass.Text == "")
            {
                MessageBox.Show("Required Fields must not be empty");
            }
            else if (txtpass.Text == txtcpass.Text)
            {
                //establishing connection to the database and opening connection
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
                con.Open();

                //creating string variable to store sql sataement
                String str = "Select username From Users where username = '" + txtuname.Text + "'  ";

                //creating sql command object to execute the query
                SqlCommand cmd1 = new SqlCommand(str, con);

                //creating data apdater obj and fetching the data into the datatable
                SqlDataAdapter sda = new SqlDataAdapter(str, con);
                DataTable dt = new DataTable();
                sda.Fill(dt);

                //if the count of rows is more than one username already exists
                if (dt.Rows.Count > 0)
                {

                    MessageBox.Show("Username already exists");
                    clear();

                }
                else
                {
                    //creating a new command of sql and defining it's type as stored procedur
                    SqlCommand cmd = new SqlCommand("UserAdd", con);
                    cmd.CommandType = CommandType.StoredProcedure;

                    //adding parameters to the stored procedure
                    cmd.Parameters.AddWithValue("@username", txtuname.Text.Trim());
                    cmd.Parameters.AddWithValue("@password", txtpass.Text.Trim());
                    cmd.Parameters.AddWithValue("@first_name", txtfname.Text.Trim());
                    cmd.Parameters.AddWithValue("@last_name", txtlname.Text.Trim());
                    cmd.Parameters.AddWithValue("@email", txtemail.Text.Trim());
                    cmd.Parameters.AddWithValue("@address", txtadd.Text.Trim());
                    cmd.Parameters.AddWithValue("@number", txtcont.Text.Trim());
                    cmd.Parameters.AddWithValue("@role", cmbrole.Text.Trim());

                    //executing the query
                    cmd.ExecuteNonQuery();
                    MessageBox.Show("Registration Successfull !!");
                    clear();

                    //hiding this form 
                    this.Hide();

                    //creating object of another form
                    frmLogin log = new frmLogin();

                    //show another form
                    log.Show();
                }

            }
            else
            {
                label13.Show();
                label13.Text = "Password Mismatch";
            }
        }


        /// <summary>
        /// this method clears all the texts
        /// </summary>
        void clear()
        {
            //providing all the textbox value to nothing
            txtuname.Text = txtadd.Text = txtcont.Text = txtcpass.Text = txtemail.Text = txtfname.Text = txtlname.Text = txtpass.Text = cmbrole.Text = "";
        }

        /// <summary>
        /// this method is called when link lable is clicked
        /// this method closes the current form and opens the login form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            //hiding this form 
            this.Hide();

            //creating object of another form
            frmLogin log = new frmLogin();

            //show another form
            log.Show();
        }
    }
}
