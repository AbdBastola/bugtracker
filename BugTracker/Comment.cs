﻿using System;
using System.Data;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace BugTracker
{
    /// <summary>
    /// this class comments contains methods for this forms
    /// </summary>
    public partial class Comment : Form
    {
        private string fname = "";
        
        /// <summary>
        /// it is a constructor of the class comment
        /// </summary>
        /// <param name="name"></param>
        public Comment(string name)
        {
            fname = name;
            InitializeComponent();

        }

        /// <summary>
        /// this method is called while loading the form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Comment_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugsDataSet1.Bugs' table. You can move, or remove it, as needed.
            this.bugsTableAdapter.Fill(this.bugsDataSet1.Bugs);
           
        }
           
        /// <summary>
        /// this method is called when the selection in combobox is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void comboBox1_SelectedValueChanged(object sender, EventArgs e)
        {
            fillComments();
            fillStatus();
          
        }
        
        /// <summary>
        /// this method connects with database to retrieve data from the datatable
        /// </summary>
        void fillStatus()
        {

            //connecting to the database using sqlconnection and opening the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //creating a string variable to store the sql statement 
            String str = "Select status From Bugs Where Id = '" + Convert.ToInt32(cmbBugs.SelectedValue) + "'";

            //a sql command classs which uses sql statement and connection
            SqlCommand cmd1 = new SqlCommand(str, con);

            //creating object of data adatper to load the value from the query and filling it into the datatable.
            SqlDataAdapter sda = new SqlDataAdapter(str, con);
            DataTable dt = new DataTable();
            sda.Fill(dt);
            if (dt.Rows.Count > 0)
            {
               
                if (dt.Rows[0][0].ToString() == "Finished")
                {
                    textComment.Enabled = false;
                    buttonComment.Enabled = false;
                }
                else
                {
                    textComment.Enabled = true;
                    buttonComment.Enabled = true;
                }


            }
        }

        /// <summary>
        /// this method fetch data from the database 
        /// </summary>
        void fillComments()
        {
            //clearing the values of textbox
            clear();
            //connecting to the databse using SqlConnection class and opening the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //creating an object of sqlcommand
            SqlCommand cmd = new SqlCommand("SelectComment", con);

            //declaring commandtype as stored procedure and adding parameters to it.
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.Parameters.AddWithValue("@bugs_id", Convert.ToInt32(cmbBugs.SelectedValue));

            //Creatiing object of data table and loadinh the table
            DataTable dt = new DataTable();
            dt.Load(cmd.ExecuteReader());

            //using for each loop  to get data table rows value
            foreach (DataRow row in dt.Rows)
            {
                listComments.Items.Add(row["posted_by"].ToString()+":- "+ row["comments"].ToString());
               
                }
            
        }

       
        /// <summary>
        /// this method is called when comment button is clicked
        /// this method enables to insert data into database using stored procedure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void buttonComment_Click(object sender, EventArgs e)
        {
            //connecting to the databse using SqlConnection class and opening the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //creating an object of sqlcommand
            SqlCommand cmd = new SqlCommand("InsertComment", con);

            //declaring commandtype as stored procedure
            cmd.CommandType = CommandType.StoredProcedure;

            //adding parameters for the command query and executing it
            cmd.Parameters.AddWithValue("@bugs_id", Convert.ToInt32(cmbBugs.SelectedValue));
            cmd.Parameters.AddWithValue("@comments", textComment.Text.Trim());
            cmd.Parameters.AddWithValue("posted_by", fname);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Commented Succesfully");
            fillComments();
        }

        /// <summary>
        /// this method clear the listbox items and texts
        /// </summary>
        void clear()
        {
            listComments.Items.Clear();
            textComment.Text = "";
        }
    }
    

}



/// <summary>
/// this class contains getter setter method for dapper
/// </summary>
class comment
{
    public int Id { get; set; }
    public int bugs_id { get; set; }
    public string posted_by { get; set; }
    public string comments { get; set; }
   
}
