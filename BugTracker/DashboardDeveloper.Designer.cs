﻿namespace BugTracker
{
    partial class DashboardDeveloper
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(DashboardDeveloper));
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle1 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle17 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle2 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle3 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle4 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle5 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle6 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle7 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle8 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle9 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle10 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle11 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle12 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle13 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle14 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle15 = new System.Windows.Forms.DataGridViewCellStyle();
            System.Windows.Forms.DataGridViewCellStyle dataGridViewCellStyle16 = new System.Windows.Forms.DataGridViewCellStyle();
            this.toolTip = new System.Windows.Forms.ToolTip(this.components);
            this.fileMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.openToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.saveToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.saveAsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.exitToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.projectsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.viewMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.toolBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.statusBarToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.helpMenu = new System.Windows.Forms.ToolStripMenuItem();
            this.contentsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.indexToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.searchToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.aboutToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip = new System.Windows.Forms.MenuStrip();
            this.bugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.addBugs = new System.Windows.Forms.ToolStripMenuItem();
            this.viewBugsToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.commentToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.profileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.editProfileToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.logoutToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.saveToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.helpToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.toolStrip = new System.Windows.Forms.ToolStrip();
            this.openToolStripButton = new System.Windows.Forms.ToolStripButton();
            this.Status = new System.Windows.Forms.ToolStripStatusLabel();
            this.statusStrip = new System.Windows.Forms.StatusStrip();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbStatus = new System.Windows.Forms.ComboBox();
            this.numberLabel = new System.Windows.Forms.Label();
            this.dgvproject = new System.Windows.Forms.DataGridView();
            this.txtProjectID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStart = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtEnd = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPriority = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDeveloper = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTester = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBy = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.txtSearch = new System.Windows.Forms.TextBox();
            this.btnSearch = new System.Windows.Forms.Button();
            this.txtRead = new System.Windows.Forms.RichTextBox();
            this.bugcoment = new System.Windows.Forms.RichTextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.cmbTitle = new System.Windows.Forms.ComboBox();
            this.projectsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.bugsDataSet = new BugTracker.bugsDataSet();
            this.panelbug = new System.Windows.Forms.Panel();
            this.cmbPriority = new System.Windows.Forms.ComboBox();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLine = new System.Windows.Forms.TextBox();
            this.label13 = new System.Windows.Forms.Label();
            this.txtMethod = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.txtLink = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.txtClass = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.txtFile = new System.Windows.Forms.TextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.tby = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.btnAddbug = new System.Windows.Forms.Button();
            this.label6 = new System.Windows.Forms.Label();
            this.txtBug = new System.Windows.Forms.TextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.butSearch = new System.Windows.Forms.Button();
            this.textSearch = new System.Windows.Forms.TextBox();
            this.projectsTableAdapter = new BugTracker.bugsDataSetTableAdapters.ProjectsTableAdapter();
            this.errorProvider1 = new System.Windows.Forms.ErrorProvider(this.components);
            this.dgvBugs = new System.Windows.Forms.DataGridView();
            this.txtBugId = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPtitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtBtitle = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtDev = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtTest = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtMan = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtStat = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtPriorit = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtfilename = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtclassname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtmethodname = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtlinenum = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtlin = new System.Windows.Forms.DataGridViewLinkColumn();
            this.txtsdate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.txtedate = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.comboStat = new System.Windows.Forms.ComboBox();
            this.label8 = new System.Windows.Forms.Label();
            this.butUpdate = new System.Windows.Forms.Button();
            this.menuStrip.SuspendLayout();
            this.toolStrip.SuspendLayout();
            this.statusStrip.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvproject)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsDataSet)).BeginInit();
            this.panelbug.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBugs)).BeginInit();
            this.SuspendLayout();
            // 
            // fileMenu
            // 
            this.fileMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripMenuItem,
            this.toolStripSeparator3,
            this.saveToolStripMenuItem,
            this.saveAsToolStripMenuItem,
            this.toolStripSeparator4,
            this.toolStripSeparator5,
            this.exitToolStripMenuItem});
            this.fileMenu.ImageTransparentColor = System.Drawing.SystemColors.ActiveBorder;
            this.fileMenu.Name = "fileMenu";
            this.fileMenu.Size = new System.Drawing.Size(37, 20);
            this.fileMenu.Text = "&File";
            // 
            // openToolStripMenuItem
            // 
            this.openToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripMenuItem.Image")));
            this.openToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.openToolStripMenuItem.Name = "openToolStripMenuItem";
            this.openToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.O)));
            this.openToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.openToolStripMenuItem.Text = "&Open";
            this.openToolStripMenuItem.Click += new System.EventHandler(this.OpenFile);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(143, 6);
            // 
            // saveToolStripMenuItem
            // 
            this.saveToolStripMenuItem.Enabled = false;
            this.saveToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripMenuItem.Image")));
            this.saveToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripMenuItem.Name = "saveToolStripMenuItem";
            this.saveToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.S)));
            this.saveToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveToolStripMenuItem.Text = "&Save";
            this.saveToolStripMenuItem.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // saveAsToolStripMenuItem
            // 
            this.saveAsToolStripMenuItem.Enabled = false;
            this.saveAsToolStripMenuItem.Name = "saveAsToolStripMenuItem";
            this.saveAsToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.saveAsToolStripMenuItem.Text = "Save &As";
            this.saveAsToolStripMenuItem.Click += new System.EventHandler(this.SaveAsToolStripMenuItem_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(143, 6);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(143, 6);
            // 
            // exitToolStripMenuItem
            // 
            this.exitToolStripMenuItem.Name = "exitToolStripMenuItem";
            this.exitToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.exitToolStripMenuItem.Text = "E&xit";
            this.exitToolStripMenuItem.Click += new System.EventHandler(this.ExitToolsStripMenuItem_Click);
            // 
            // projectsToolStripMenuItem
            // 
            this.projectsToolStripMenuItem.Name = "projectsToolStripMenuItem";
            this.projectsToolStripMenuItem.Size = new System.Drawing.Size(61, 20);
            this.projectsToolStripMenuItem.Text = "&Projects";
            this.projectsToolStripMenuItem.Click += new System.EventHandler(this.projectsToolStripMenuItem_Click);
            // 
            // viewMenu
            // 
            this.viewMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolBarToolStripMenuItem,
            this.statusBarToolStripMenuItem});
            this.viewMenu.Name = "viewMenu";
            this.viewMenu.Size = new System.Drawing.Size(44, 20);
            this.viewMenu.Text = "&View";
            // 
            // toolBarToolStripMenuItem
            // 
            this.toolBarToolStripMenuItem.Checked = true;
            this.toolBarToolStripMenuItem.CheckOnClick = true;
            this.toolBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.toolBarToolStripMenuItem.Name = "toolBarToolStripMenuItem";
            this.toolBarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.toolBarToolStripMenuItem.Text = "&Toolbar";
            this.toolBarToolStripMenuItem.Click += new System.EventHandler(this.ToolBarToolStripMenuItem_Click);
            // 
            // statusBarToolStripMenuItem
            // 
            this.statusBarToolStripMenuItem.Checked = true;
            this.statusBarToolStripMenuItem.CheckOnClick = true;
            this.statusBarToolStripMenuItem.CheckState = System.Windows.Forms.CheckState.Checked;
            this.statusBarToolStripMenuItem.Name = "statusBarToolStripMenuItem";
            this.statusBarToolStripMenuItem.Size = new System.Drawing.Size(126, 22);
            this.statusBarToolStripMenuItem.Text = "&Status Bar";
            this.statusBarToolStripMenuItem.Click += new System.EventHandler(this.StatusBarToolStripMenuItem_Click);
            // 
            // helpMenu
            // 
            this.helpMenu.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.contentsToolStripMenuItem,
            this.indexToolStripMenuItem,
            this.searchToolStripMenuItem,
            this.toolStripSeparator8,
            this.aboutToolStripMenuItem});
            this.helpMenu.Name = "helpMenu";
            this.helpMenu.Size = new System.Drawing.Size(44, 20);
            this.helpMenu.Text = "&Help";
            // 
            // contentsToolStripMenuItem
            // 
            this.contentsToolStripMenuItem.Name = "contentsToolStripMenuItem";
            this.contentsToolStripMenuItem.ShortcutKeys = ((System.Windows.Forms.Keys)((System.Windows.Forms.Keys.Control | System.Windows.Forms.Keys.F1)));
            this.contentsToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.contentsToolStripMenuItem.Text = "&Contents";
            // 
            // indexToolStripMenuItem
            // 
            this.indexToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("indexToolStripMenuItem.Image")));
            this.indexToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.indexToolStripMenuItem.Name = "indexToolStripMenuItem";
            this.indexToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.indexToolStripMenuItem.Text = "&Index";
            // 
            // searchToolStripMenuItem
            // 
            this.searchToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("searchToolStripMenuItem.Image")));
            this.searchToolStripMenuItem.ImageTransparentColor = System.Drawing.Color.Black;
            this.searchToolStripMenuItem.Name = "searchToolStripMenuItem";
            this.searchToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.searchToolStripMenuItem.Text = "&Search";
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(165, 6);
            // 
            // aboutToolStripMenuItem
            // 
            this.aboutToolStripMenuItem.Name = "aboutToolStripMenuItem";
            this.aboutToolStripMenuItem.Size = new System.Drawing.Size(168, 22);
            this.aboutToolStripMenuItem.Text = "&About ... ...";
            // 
            // menuStrip
            // 
            this.menuStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileMenu,
            this.bugsToolStripMenuItem,
            this.projectsToolStripMenuItem,
            this.viewMenu,
            this.helpMenu,
            this.profileToolStripMenuItem});
            this.menuStrip.Location = new System.Drawing.Point(0, 0);
            this.menuStrip.Name = "menuStrip";
            this.menuStrip.Size = new System.Drawing.Size(1320, 24);
            this.menuStrip.TabIndex = 0;
            this.menuStrip.Text = "MenuStrip";
            // 
            // bugsToolStripMenuItem
            // 
            this.bugsToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.addBugs,
            this.viewBugsToolStripMenuItem,
            this.commentToolStripMenuItem});
            this.bugsToolStripMenuItem.Name = "bugsToolStripMenuItem";
            this.bugsToolStripMenuItem.Size = new System.Drawing.Size(45, 20);
            this.bugsToolStripMenuItem.Text = "&Bugs";
            // 
            // addBugs
            // 
            this.addBugs.Enabled = false;
            this.addBugs.Name = "addBugs";
            this.addBugs.Size = new System.Drawing.Size(131, 22);
            this.addBugs.Text = "Add Bugs";
            this.addBugs.Click += new System.EventHandler(this.addBugs_Click);
            // 
            // viewBugsToolStripMenuItem
            // 
            this.viewBugsToolStripMenuItem.Name = "viewBugsToolStripMenuItem";
            this.viewBugsToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.viewBugsToolStripMenuItem.Text = "View Bugs";
            this.viewBugsToolStripMenuItem.Click += new System.EventHandler(this.viewBugsToolStripMenuItem_Click);
            // 
            // commentToolStripMenuItem
            // 
            this.commentToolStripMenuItem.Name = "commentToolStripMenuItem";
            this.commentToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.commentToolStripMenuItem.Text = "Comment ";
            this.commentToolStripMenuItem.Click += new System.EventHandler(this.commentToolStripMenuItem_Click);
            // 
            // profileToolStripMenuItem
            // 
            this.profileToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.editProfileToolStripMenuItem,
            this.logoutToolStripMenuItem1});
            this.profileToolStripMenuItem.Name = "profileToolStripMenuItem";
            this.profileToolStripMenuItem.Size = new System.Drawing.Size(53, 20);
            this.profileToolStripMenuItem.Text = "&Profile";
            // 
            // editProfileToolStripMenuItem
            // 
            this.editProfileToolStripMenuItem.Name = "editProfileToolStripMenuItem";
            this.editProfileToolStripMenuItem.Size = new System.Drawing.Size(131, 22);
            this.editProfileToolStripMenuItem.Text = "Edit Profile";
            this.editProfileToolStripMenuItem.Click += new System.EventHandler(this.editProfileToolStripMenuItem_Click);
            // 
            // logoutToolStripMenuItem1
            // 
            this.logoutToolStripMenuItem1.Name = "logoutToolStripMenuItem1";
            this.logoutToolStripMenuItem1.Size = new System.Drawing.Size(131, 22);
            this.logoutToolStripMenuItem1.Text = "Logout";
            this.logoutToolStripMenuItem1.Click += new System.EventHandler(this.logoutToolStripMenuItem1_Click);
            // 
            // saveToolStripButton
            // 
            this.saveToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.saveToolStripButton.Enabled = false;
            this.saveToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("saveToolStripButton.Image")));
            this.saveToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.saveToolStripButton.Name = "saveToolStripButton";
            this.saveToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.saveToolStripButton.Text = "Save";
            this.saveToolStripButton.Click += new System.EventHandler(this.saveToolStripMenuItem_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // helpToolStripButton
            // 
            this.helpToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.helpToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("helpToolStripButton.Image")));
            this.helpToolStripButton.ImageTransparentColor = System.Drawing.Color.Black;
            this.helpToolStripButton.Name = "helpToolStripButton";
            this.helpToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.helpToolStripButton.Text = "Help";
            // 
            // toolStrip
            // 
            this.toolStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.openToolStripButton,
            this.saveToolStripButton,
            this.toolStripSeparator1,
            this.helpToolStripButton});
            this.toolStrip.Location = new System.Drawing.Point(0, 24);
            this.toolStrip.Name = "toolStrip";
            this.toolStrip.Size = new System.Drawing.Size(1320, 25);
            this.toolStrip.TabIndex = 1;
            this.toolStrip.Text = "ToolStrip";
            // 
            // openToolStripButton
            // 
            this.openToolStripButton.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.openToolStripButton.Image = ((System.Drawing.Image)(resources.GetObject("openToolStripButton.Image")));
            this.openToolStripButton.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.openToolStripButton.Name = "openToolStripButton";
            this.openToolStripButton.Size = new System.Drawing.Size(23, 22);
            this.openToolStripButton.Text = "Open";
            // 
            // Status
            // 
            this.Status.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Status.Name = "Status";
            this.Status.Size = new System.Drawing.Size(57, 21);
            this.Status.Text = "Status";
            // 
            // statusStrip
            // 
            this.statusStrip.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.Status});
            this.statusStrip.Location = new System.Drawing.Point(0, 683);
            this.statusStrip.Name = "statusStrip";
            this.statusStrip.Size = new System.Drawing.Size(1320, 26);
            this.statusStrip.TabIndex = 2;
            this.statusStrip.Text = "StatusStrip";
            // 
            // label1
            // 
            this.label1.BackColor = System.Drawing.SystemColors.AppWorkspace;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(1120, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(154, 29);
            this.label1.TabIndex = 6;
            this.label1.Text = "label1";
            // 
            // label2
            // 
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(94, 64);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(60, 26);
            this.label2.TabIndex = 14;
            this.label2.Text = "Status";
            // 
            // cmbStatus
            // 
            this.cmbStatus.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbStatus.FormattingEnabled = true;
            this.cmbStatus.Location = new System.Drawing.Point(160, 64);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(141, 24);
            this.cmbStatus.TabIndex = 15;
            // 
            // numberLabel
            // 
            this.numberLabel.Font = new System.Drawing.Font("Courier New", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.numberLabel.Location = new System.Drawing.Point(2, 50);
            this.numberLabel.Name = "numberLabel";
            this.numberLabel.Padding = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.numberLabel.Size = new System.Drawing.Size(46, 636);
            this.numberLabel.TabIndex = 23;
            this.numberLabel.Text = "1";
            this.numberLabel.TextAlign = System.Drawing.ContentAlignment.TopRight;
            this.numberLabel.Visible = false;
            // 
            // dgvproject
            // 
            this.dgvproject.BackgroundColor = System.Drawing.SystemColors.ButtonFace;
            this.dgvproject.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvproject.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtProjectID,
            this.txtTitle,
            this.Column1,
            this.txtStart,
            this.txtEnd,
            this.txtPriority,
            this.txtDeveloper,
            this.txtTester,
            this.txtBy,
            this.txtStatus});
            this.dgvproject.Location = new System.Drawing.Point(27, 106);
            this.dgvproject.Name = "dgvproject";
            this.dgvproject.Size = new System.Drawing.Size(1257, 555);
            this.dgvproject.TabIndex = 18;
            this.dgvproject.Visible = false;
            this.dgvproject.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvproject_CellDoubleClick);
            // 
            // txtProjectID
            // 
            this.txtProjectID.DataPropertyName = "Id";
            this.txtProjectID.HeaderText = "projectID";
            this.txtProjectID.Name = "txtProjectID";
            this.txtProjectID.ReadOnly = true;
            this.txtProjectID.Visible = false;
            // 
            // txtTitle
            // 
            this.txtTitle.DataPropertyName = "Title";
            this.txtTitle.HeaderText = "Project Title";
            this.txtTitle.Name = "txtTitle";
            this.txtTitle.ReadOnly = true;
            this.txtTitle.Width = 150;
            // 
            // Column1
            // 
            this.Column1.DataPropertyName = "Description";
            this.Column1.HeaderText = "Project Description";
            this.Column1.Name = "Column1";
            this.Column1.ReadOnly = true;
            this.Column1.Width = 360;
            // 
            // txtStart
            // 
            this.txtStart.DataPropertyName = "Start_Date";
            this.txtStart.HeaderText = "Start Date";
            this.txtStart.Name = "txtStart";
            this.txtStart.ReadOnly = true;
            // 
            // txtEnd
            // 
            this.txtEnd.DataPropertyName = "End_Date";
            this.txtEnd.HeaderText = "End Date";
            this.txtEnd.Name = "txtEnd";
            this.txtEnd.ReadOnly = true;
            // 
            // txtPriority
            // 
            this.txtPriority.DataPropertyName = "Priority";
            this.txtPriority.HeaderText = "Priority";
            this.txtPriority.Name = "txtPriority";
            this.txtPriority.ReadOnly = true;
            this.txtPriority.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // txtDeveloper
            // 
            this.txtDeveloper.DataPropertyName = "Developer";
            this.txtDeveloper.HeaderText = "Assinged Developer";
            this.txtDeveloper.Name = "txtDeveloper";
            this.txtDeveloper.ReadOnly = true;
            this.txtDeveloper.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // txtTester
            // 
            this.txtTester.DataPropertyName = "Tester";
            this.txtTester.HeaderText = "Assinged Tester";
            this.txtTester.Name = "txtTester";
            this.txtTester.ReadOnly = true;
            this.txtTester.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // txtBy
            // 
            this.txtBy.DataPropertyName = "Manager";
            this.txtBy.HeaderText = "Assinged By";
            this.txtBy.Name = "txtBy";
            this.txtBy.ReadOnly = true;
            // 
            // txtStatus
            // 
            this.txtStatus.DataPropertyName = "Status";
            this.txtStatus.HeaderText = "Status";
            this.txtStatus.Name = "txtStatus";
            this.txtStatus.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            // 
            // btnUpdate
            // 
            this.btnUpdate.BackColor = System.Drawing.Color.CornflowerBlue;
            this.btnUpdate.Location = new System.Drawing.Point(307, 64);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(93, 26);
            this.btnUpdate.TabIndex = 16;
            this.btnUpdate.Text = "Update";
            this.btnUpdate.UseVisualStyleBackColor = false;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // txtSearch
            // 
            this.txtSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSearch.Location = new System.Drawing.Point(885, 74);
            this.txtSearch.Name = "txtSearch";
            this.txtSearch.Size = new System.Drawing.Size(176, 26);
            this.txtSearch.TabIndex = 11;
            this.txtSearch.Visible = false;
            // 
            // btnSearch
            // 
            this.btnSearch.BackColor = System.Drawing.Color.DarkRed;
            this.btnSearch.ForeColor = System.Drawing.SystemColors.Control;
            this.btnSearch.Location = new System.Drawing.Point(1094, 70);
            this.btnSearch.Name = "btnSearch";
            this.btnSearch.Size = new System.Drawing.Size(88, 30);
            this.btnSearch.TabIndex = 12;
            this.btnSearch.Text = "Search";
            this.btnSearch.UseVisualStyleBackColor = false;
            this.btnSearch.Visible = false;
            this.btnSearch.Click += new System.EventHandler(this.btnSearch_Click);
            // 
            // txtRead
            // 
            this.txtRead.BackColor = System.Drawing.SystemColors.Window;
            this.txtRead.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRead.Location = new System.Drawing.Point(45, 49);
            this.txtRead.Name = "txtRead";
            this.txtRead.Size = new System.Drawing.Size(748, 630);
            this.txtRead.TabIndex = 21;
            this.txtRead.Text = "";
            this.txtRead.VScroll += new System.EventHandler(this.richTextBox1_VScroll);
            this.txtRead.FontChanged += new System.EventHandler(this.richTextBox1_FontChanged);
            this.txtRead.TextChanged += new System.EventHandler(this.richTextBox1_TextChanged);
            this.txtRead.Resize += new System.EventHandler(this.richTextBox1_Resize);
            // 
            // bugcoment
            // 
            this.bugcoment.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.bugcoment.Location = new System.Drawing.Point(24, 290);
            this.bugcoment.Name = "bugcoment";
            this.bugcoment.Size = new System.Drawing.Size(451, 109);
            this.bugcoment.TabIndex = 25;
            this.bugcoment.Text = "";
            // 
            // label3
            // 
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 14.25F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(152, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(209, 23);
            this.label3.TabIndex = 26;
            this.label3.Text = "Post/Comment Bugs";
            // 
            // cmbTitle
            // 
            this.cmbTitle.DataSource = this.projectsBindingSource;
            this.cmbTitle.DisplayMember = "title";
            this.cmbTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbTitle.FormattingEnabled = true;
            this.cmbTitle.Location = new System.Drawing.Point(35, 70);
            this.cmbTitle.Name = "cmbTitle";
            this.cmbTitle.Size = new System.Drawing.Size(207, 26);
            this.cmbTitle.TabIndex = 27;
            this.cmbTitle.ValueMember = "Id";
            // 
            // projectsBindingSource
            // 
            this.projectsBindingSource.DataMember = "Projects";
            this.projectsBindingSource.DataSource = this.bugsDataSet;
            // 
            // bugsDataSet
            // 
            this.bugsDataSet.DataSetName = "bugsDataSet";
            this.bugsDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // panelbug
            // 
            this.panelbug.Controls.Add(this.cmbPriority);
            this.panelbug.Controls.Add(this.label14);
            this.panelbug.Controls.Add(this.txtLine);
            this.panelbug.Controls.Add(this.label13);
            this.panelbug.Controls.Add(this.txtMethod);
            this.panelbug.Controls.Add(this.label12);
            this.panelbug.Controls.Add(this.txtLink);
            this.panelbug.Controls.Add(this.label11);
            this.panelbug.Controls.Add(this.txtClass);
            this.panelbug.Controls.Add(this.label10);
            this.panelbug.Controls.Add(this.txtFile);
            this.panelbug.Controls.Add(this.label9);
            this.panelbug.Controls.Add(this.tby);
            this.panelbug.Controls.Add(this.label7);
            this.panelbug.Controls.Add(this.btnAddbug);
            this.panelbug.Controls.Add(this.label6);
            this.panelbug.Controls.Add(this.txtBug);
            this.panelbug.Controls.Add(this.label5);
            this.panelbug.Controls.Add(this.label4);
            this.panelbug.Controls.Add(this.cmbTitle);
            this.panelbug.Controls.Add(this.bugcoment);
            this.panelbug.Controls.Add(this.label3);
            this.panelbug.Location = new System.Drawing.Point(793, 106);
            this.panelbug.Name = "panelbug";
            this.panelbug.Size = new System.Drawing.Size(508, 573);
            this.panelbug.TabIndex = 28;
            this.panelbug.Visible = false;
            // 
            // cmbPriority
            // 
            this.cmbPriority.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbPriority.FormattingEnabled = true;
            this.cmbPriority.Items.AddRange(new object[] {
            "High",
            "Medium ",
            "Low"});
            this.cmbPriority.Location = new System.Drawing.Point(24, 508);
            this.cmbPriority.Name = "cmbPriority";
            this.cmbPriority.Size = new System.Drawing.Size(190, 26);
            this.cmbPriority.TabIndex = 46;
            // 
            // label14
            // 
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(21, 488);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(210, 26);
            this.label14.TabIndex = 45;
            this.label14.Text = "Priority";
            // 
            // txtLine
            // 
            this.txtLine.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLine.Location = new System.Drawing.Point(268, 225);
            this.txtLine.Name = "txtLine";
            this.txtLine.Size = new System.Drawing.Size(207, 24);
            this.txtLine.TabIndex = 44;
            // 
            // label13
            // 
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(265, 196);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(210, 26);
            this.label13.TabIndex = 43;
            this.label13.Text = "Line Number";
            // 
            // txtMethod
            // 
            this.txtMethod.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMethod.Location = new System.Drawing.Point(35, 225);
            this.txtMethod.Name = "txtMethod";
            this.txtMethod.Size = new System.Drawing.Size(207, 24);
            this.txtMethod.TabIndex = 42;
            // 
            // label12
            // 
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(32, 196);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(210, 26);
            this.label12.TabIndex = 41;
            this.label12.Text = "Method Name";
            // 
            // txtLink
            // 
            this.txtLink.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLink.Location = new System.Drawing.Point(259, 442);
            this.txtLink.Name = "txtLink";
            this.txtLink.Size = new System.Drawing.Size(207, 24);
            this.txtLink.TabIndex = 40;
            // 
            // label11
            // 
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(256, 413);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(210, 26);
            this.label11.TabIndex = 39;
            this.label11.Text = "Git Link";
            // 
            // txtClass
            // 
            this.txtClass.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtClass.Location = new System.Drawing.Point(268, 139);
            this.txtClass.Name = "txtClass";
            this.txtClass.Size = new System.Drawing.Size(207, 24);
            this.txtClass.TabIndex = 38;
            // 
            // label10
            // 
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(265, 110);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(210, 26);
            this.label10.TabIndex = 37;
            this.label10.Text = "Class Name";
            // 
            // txtFile
            // 
            this.txtFile.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFile.Location = new System.Drawing.Point(268, 70);
            this.txtFile.Name = "txtFile";
            this.txtFile.Size = new System.Drawing.Size(207, 24);
            this.txtFile.TabIndex = 36;
            // 
            // label9
            // 
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(265, 41);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(210, 26);
            this.label9.TabIndex = 35;
            this.label9.Text = "File Name";
            // 
            // tby
            // 
            this.tby.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tby.Location = new System.Drawing.Point(26, 442);
            this.tby.Name = "tby";
            this.tby.Size = new System.Drawing.Size(188, 24);
            this.tby.TabIndex = 34;
            // 
            // label7
            // 
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(23, 413);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(210, 26);
            this.label7.TabIndex = 33;
            this.label7.Text = "Posted By";
            // 
            // btnAddbug
            // 
            this.btnAddbug.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAddbug.Location = new System.Drawing.Point(291, 525);
            this.btnAddbug.Name = "btnAddbug";
            this.btnAddbug.Size = new System.Drawing.Size(142, 29);
            this.btnAddbug.TabIndex = 32;
            this.btnAddbug.Text = "Add Bugs";
            this.btnAddbug.UseVisualStyleBackColor = true;
            this.btnAddbug.Click += new System.EventHandler(this.btnAddbug_Click);
            // 
            // label6
            // 
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(32, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(210, 26);
            this.label6.TabIndex = 31;
            this.label6.Text = "Description";
            // 
            // txtBug
            // 
            this.txtBug.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBug.Location = new System.Drawing.Point(35, 139);
            this.txtBug.Name = "txtBug";
            this.txtBug.Size = new System.Drawing.Size(207, 24);
            this.txtBug.TabIndex = 30;
            // 
            // label5
            // 
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(32, 110);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(210, 26);
            this.label5.TabIndex = 29;
            this.label5.Text = "Bugs";
            // 
            // label4
            // 
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(32, 41);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(210, 26);
            this.label4.TabIndex = 28;
            this.label4.Text = "Project Title";
            // 
            // butSearch
            // 
            this.butSearch.Location = new System.Drawing.Point(289, 123);
            this.butSearch.Name = "butSearch";
            this.butSearch.Size = new System.Drawing.Size(73, 27);
            this.butSearch.TabIndex = 36;
            this.butSearch.Text = "Search";
            this.butSearch.UseVisualStyleBackColor = true;
            this.butSearch.Visible = false;
            this.butSearch.Click += new System.EventHandler(this.butSearch_Click);
            // 
            // textSearch
            // 
            this.textSearch.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textSearch.Location = new System.Drawing.Point(97, 128);
            this.textSearch.Name = "textSearch";
            this.textSearch.Size = new System.Drawing.Size(186, 22);
            this.textSearch.TabIndex = 35;
            this.textSearch.Visible = false;
            // 
            // projectsTableAdapter
            // 
            this.projectsTableAdapter.ClearBeforeFill = true;
            // 
            // errorProvider1
            // 
            this.errorProvider1.ContainerControl = this;
            // 
            // dgvBugs
            // 
            this.dgvBugs.BackgroundColor = System.Drawing.SystemColors.Control;
            this.dgvBugs.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D;
            dataGridViewCellStyle1.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft;
            dataGridViewCellStyle1.BackColor = System.Drawing.SystemColors.Control;
            dataGridViewCellStyle1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            dataGridViewCellStyle1.ForeColor = System.Drawing.SystemColors.WindowText;
            dataGridViewCellStyle1.SelectionBackColor = System.Drawing.SystemColors.Highlight;
            dataGridViewCellStyle1.SelectionForeColor = System.Drawing.SystemColors.HighlightText;
            dataGridViewCellStyle1.WrapMode = System.Windows.Forms.DataGridViewTriState.True;
            this.dgvBugs.ColumnHeadersDefaultCellStyle = dataGridViewCellStyle1;
            this.dgvBugs.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvBugs.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.txtBugId,
            this.txtPtitle,
            this.txtBtitle,
            this.txtDev,
            this.txtTest,
            this.txtMan,
            this.txtStat,
            this.txtPriorit,
            this.txtfilename,
            this.txtclassname,
            this.txtmethodname,
            this.txtlinenum,
            this.txtlin,
            this.txtsdate,
            this.txtedate});
            this.dgvBugs.Cursor = System.Windows.Forms.Cursors.Default;
            this.dgvBugs.Enabled = false;
            this.dgvBugs.Location = new System.Drawing.Point(26, 156);
            this.dgvBugs.Name = "dgvBugs";
            this.dgvBugs.ReadOnly = true;
            dataGridViewCellStyle17.BackColor = System.Drawing.Color.FloralWhite;
            this.dgvBugs.RowsDefaultCellStyle = dataGridViewCellStyle17;
            this.dgvBugs.Size = new System.Drawing.Size(1272, 504);
            this.dgvBugs.TabIndex = 30;
            this.dgvBugs.Visible = false;
            this.dgvBugs.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvBugs_CellDoubleClick);
            // 
            // txtBugId
            // 
            this.txtBugId.DataPropertyName = "Id";
            dataGridViewCellStyle2.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBugId.DefaultCellStyle = dataGridViewCellStyle2;
            this.txtBugId.HeaderText = "Bug ID";
            this.txtBugId.Name = "txtBugId";
            this.txtBugId.ReadOnly = true;
            // 
            // txtPtitle
            // 
            this.txtPtitle.DataPropertyName = "title";
            dataGridViewCellStyle3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPtitle.DefaultCellStyle = dataGridViewCellStyle3;
            this.txtPtitle.HeaderText = "Project Title";
            this.txtPtitle.Name = "txtPtitle";
            this.txtPtitle.ReadOnly = true;
            this.txtPtitle.Width = 200;
            // 
            // txtBtitle
            // 
            this.txtBtitle.DataPropertyName = "bug_title";
            dataGridViewCellStyle4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtBtitle.DefaultCellStyle = dataGridViewCellStyle4;
            this.txtBtitle.HeaderText = "Bug Title";
            this.txtBtitle.Name = "txtBtitle";
            this.txtBtitle.ReadOnly = true;
            this.txtBtitle.Width = 200;
            // 
            // txtDev
            // 
            this.txtDev.DataPropertyName = "developer";
            dataGridViewCellStyle5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDev.DefaultCellStyle = dataGridViewCellStyle5;
            this.txtDev.HeaderText = "Assinged Developer";
            this.txtDev.Name = "txtDev";
            this.txtDev.ReadOnly = true;
            this.txtDev.Width = 200;
            // 
            // txtTest
            // 
            this.txtTest.DataPropertyName = "tester";
            dataGridViewCellStyle6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTest.DefaultCellStyle = dataGridViewCellStyle6;
            this.txtTest.HeaderText = "Assinged Tester";
            this.txtTest.Name = "txtTest";
            this.txtTest.ReadOnly = true;
            this.txtTest.Width = 200;
            // 
            // txtMan
            // 
            this.txtMan.DataPropertyName = "manager";
            dataGridViewCellStyle7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMan.DefaultCellStyle = dataGridViewCellStyle7;
            this.txtMan.HeaderText = "Assinged Manager";
            this.txtMan.Name = "txtMan";
            this.txtMan.ReadOnly = true;
            this.txtMan.Width = 200;
            // 
            // txtStat
            // 
            this.txtStat.DataPropertyName = "status";
            dataGridViewCellStyle8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtStat.DefaultCellStyle = dataGridViewCellStyle8;
            this.txtStat.HeaderText = "Status";
            this.txtStat.Name = "txtStat";
            this.txtStat.ReadOnly = true;
            // 
            // txtPriorit
            // 
            this.txtPriorit.DataPropertyName = "priority";
            dataGridViewCellStyle9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPriorit.DefaultCellStyle = dataGridViewCellStyle9;
            this.txtPriorit.HeaderText = "Priority";
            this.txtPriorit.Name = "txtPriorit";
            this.txtPriorit.ReadOnly = true;
            // 
            // txtfilename
            // 
            this.txtfilename.DataPropertyName = "filename";
            dataGridViewCellStyle10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtfilename.DefaultCellStyle = dataGridViewCellStyle10;
            this.txtfilename.HeaderText = "File Name";
            this.txtfilename.Name = "txtfilename";
            this.txtfilename.ReadOnly = true;
            // 
            // txtclassname
            // 
            this.txtclassname.DataPropertyName = "classname";
            dataGridViewCellStyle11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtclassname.DefaultCellStyle = dataGridViewCellStyle11;
            this.txtclassname.HeaderText = "Class Name";
            this.txtclassname.Name = "txtclassname";
            this.txtclassname.ReadOnly = true;
            // 
            // txtmethodname
            // 
            this.txtmethodname.DataPropertyName = "methodname";
            dataGridViewCellStyle12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtmethodname.DefaultCellStyle = dataGridViewCellStyle12;
            this.txtmethodname.HeaderText = "Method Name";
            this.txtmethodname.Name = "txtmethodname";
            this.txtmethodname.ReadOnly = true;
            // 
            // txtlinenum
            // 
            this.txtlinenum.DataPropertyName = "linenumber";
            dataGridViewCellStyle13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlinenum.DefaultCellStyle = dataGridViewCellStyle13;
            this.txtlinenum.HeaderText = "Line Number";
            this.txtlinenum.Name = "txtlinenum";
            this.txtlinenum.ReadOnly = true;
            // 
            // txtlin
            // 
            this.txtlin.DataPropertyName = "link";
            dataGridViewCellStyle14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtlin.DefaultCellStyle = dataGridViewCellStyle14;
            this.txtlin.HeaderText = "Git Link";
            this.txtlin.LinkBehavior = System.Windows.Forms.LinkBehavior.AlwaysUnderline;
            this.txtlin.Name = "txtlin";
            this.txtlin.ReadOnly = true;
            this.txtlin.Resizable = System.Windows.Forms.DataGridViewTriState.True;
            this.txtlin.SortMode = System.Windows.Forms.DataGridViewColumnSortMode.Automatic;
            // 
            // txtsdate
            // 
            this.txtsdate.DataPropertyName = "start_time";
            dataGridViewCellStyle15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtsdate.DefaultCellStyle = dataGridViewCellStyle15;
            this.txtsdate.HeaderText = "Start Date";
            this.txtsdate.Name = "txtsdate";
            this.txtsdate.ReadOnly = true;
            // 
            // txtedate
            // 
            this.txtedate.DataPropertyName = "end_time";
            dataGridViewCellStyle16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtedate.DefaultCellStyle = dataGridViewCellStyle16;
            this.txtedate.HeaderText = "End Date";
            this.txtedate.Name = "txtedate";
            this.txtedate.ReadOnly = true;
            // 
            // comboStat
            // 
            this.comboStat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.comboStat.FormattingEnabled = true;
            this.comboStat.Items.AddRange(new object[] {
            "Started",
            "Testing",
            "Finished"});
            this.comboStat.Location = new System.Drawing.Point(457, 107);
            this.comboStat.Name = "comboStat";
            this.comboStat.Size = new System.Drawing.Size(171, 26);
            this.comboStat.TabIndex = 38;
            this.comboStat.Visible = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, ((System.Drawing.FontStyle)((System.Drawing.FontStyle.Bold | System.Drawing.FontStyle.Italic))), System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(453, 84);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(126, 20);
            this.label8.TabIndex = 39;
            this.label8.Text = "Testing Status";
            this.label8.Visible = false;
            // 
            // butUpdate
            // 
            this.butUpdate.Location = new System.Drawing.Point(634, 110);
            this.butUpdate.Name = "butUpdate";
            this.butUpdate.Size = new System.Drawing.Size(75, 23);
            this.butUpdate.TabIndex = 40;
            this.butUpdate.Text = "Update";
            this.butUpdate.UseVisualStyleBackColor = true;
            this.butUpdate.Visible = false;
            this.butUpdate.Click += new System.EventHandler(this.butUpdate_Click);
            // 
            // DashboardDeveloper
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1320, 709);
            this.Controls.Add(this.butUpdate);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.comboStat);
            this.Controls.Add(this.butSearch);
            this.Controls.Add(this.dgvBugs);
            this.Controls.Add(this.textSearch);
            this.Controls.Add(this.panelbug);
            this.Controls.Add(this.txtRead);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.btnSearch);
            this.Controls.Add(this.dgvproject);
            this.Controls.Add(this.txtSearch);
            this.Controls.Add(this.numberLabel);
            this.Controls.Add(this.cmbStatus);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.statusStrip);
            this.Controls.Add(this.toolStrip);
            this.Controls.Add(this.menuStrip);
            this.IsMdiContainer = true;
            this.MainMenuStrip = this.menuStrip;
            this.Name = "DashboardDeveloper";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "DashboardDeveloper";
            this.TopMost = true;
            this.Load += new System.EventHandler(this.DashboardDeveloper_Load);
            this.menuStrip.ResumeLayout(false);
            this.menuStrip.PerformLayout();
            this.toolStrip.ResumeLayout(false);
            this.toolStrip.PerformLayout();
            this.statusStrip.ResumeLayout(false);
            this.statusStrip.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvproject)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.projectsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.bugsDataSet)).EndInit();
            this.panelbug.ResumeLayout(false);
            this.panelbug.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.errorProvider1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvBugs)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }
        #endregion
        private System.Windows.Forms.ToolTip toolTip;
        private System.Windows.Forms.ToolStripMenuItem fileMenu;
        private System.Windows.Forms.ToolStripMenuItem openToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripMenuItem saveToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem saveAsToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripMenuItem exitToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem projectsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewMenu;
        private System.Windows.Forms.ToolStripMenuItem toolBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem statusBarToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem helpMenu;
        private System.Windows.Forms.ToolStripMenuItem contentsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem indexToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem searchToolStripMenuItem;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripMenuItem aboutToolStripMenuItem;
        private System.Windows.Forms.MenuStrip menuStrip;
        private System.Windows.Forms.ToolStripButton saveToolStripButton;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton helpToolStripButton;
        private System.Windows.Forms.ToolStrip toolStrip;
        private System.Windows.Forms.ToolStripStatusLabel Status;
        private System.Windows.Forms.StatusStrip statusStrip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.ComboBox cmbStatus;
        private System.Windows.Forms.Label numberLabel;
        private System.Windows.Forms.DataGridView dgvproject;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.TextBox txtSearch;
        private System.Windows.Forms.Button btnSearch;
        private System.Windows.Forms.RichTextBox txtRead;
        private System.Windows.Forms.ToolStripMenuItem bugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem viewBugsToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem commentToolStripMenuItem;
        private System.Windows.Forms.RichTextBox bugcoment;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.ToolStripMenuItem addBugs;
        private System.Windows.Forms.ComboBox cmbTitle;
        private System.Windows.Forms.Panel panelbug;
        private System.Windows.Forms.Button btnAddbug;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtBug;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        private bugsDataSet bugsDataSet;
        private System.Windows.Forms.BindingSource projectsBindingSource;
        private bugsDataSetTableAdapters.ProjectsTableAdapter projectsTableAdapter;
        private System.Windows.Forms.TextBox tby;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.ErrorProvider errorProvider1;
        private System.Windows.Forms.DataGridView dgvBugs;
        private System.Windows.Forms.Button butSearch;
        private System.Windows.Forms.TextBox textSearch;
        private System.Windows.Forms.Button butUpdate;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.ComboBox comboStat;
        private System.Windows.Forms.ToolStripButton openToolStripButton;
        private System.Windows.Forms.ToolStripMenuItem profileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem editProfileToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem logoutToolStripMenuItem1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtProjectID;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtStart;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtEnd;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDeveloper;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTester;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBy;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtStatus;
        private System.Windows.Forms.TextBox txtClass;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.TextBox txtFile;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtLink;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.TextBox txtLine;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtMethod;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.ComboBox cmbPriority;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBugId;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPtitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtBtitle;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtDev;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtTest;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtMan;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtStat;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtPriorit;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtfilename;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtclassname;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtmethodname;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtlinenum;
        private System.Windows.Forms.DataGridViewLinkColumn txtlin;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtsdate;
        private System.Windows.Forms.DataGridViewTextBoxColumn txtedate;
    }
}



