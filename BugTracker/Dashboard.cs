﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Microsoft.Office.Interop.Excel;
using System.Data.SqlClient;

  using Dapper;

namespace BugTracker
{

    /// <summary>
    /// this class holds all the method that is used in the form named dashboard
    /// </summary>
    public partial class Dashboard : Form
    {

        private string fullname = "";
        int Id = 0;

        /// <summary>
        /// it is the constructor od the class dashboard
        /// </summary>
        /// <param name="role"></param>
        /// <param name="name"></param>
        public Dashboard(String role, String name)
        {

            //initializing the components when loading the page
            InitializeComponent();

            //assinging role to the status
            status.Text = role;

            //assing name to the label and text
            label1.Text = "Welcome!  " + name;
            txtManager.Text = name;

            //assigning name to the variables and itemss
            fullname = name;
            editProfileToolStripMenuItem.Text = name;
        }



        /// <summary>
        /// this method will enable to close the current form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            //closing this application
            this.Close();
        }


        /// <summary>
        /// this method will help to interact with toolbar menu in dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        /// <summary>
        /// this method will help to interact with status bar menu in dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip1.Visible = statusBarToolStripMenuItem.Checked;
        }

        /// <summary>
        /// this method is called when the form dashboard is loaded
        /// it will enables all the controls and other methods when the form is loaded
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dashboard_Load(object sender, EventArgs e)
        {
            fillData();
            fillcombo();
            fillcomboTest();
            clear();
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            fillBugs();


        }


        /// <summary>
        /// this method connects with the datase to retrieve the data of project table
        /// using dapper it will store the data into list[] view and assing it to datagrid
        /// </summary>
        void fillData()
        {
            //connecting to the database
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");

            //using dyaminc parameters object of dapper extension
            DynamicParameters param = new DynamicParameters();

            //adding parameter to the procrdure
            param.Add("@SearchText", txtSearch.Text.Trim());

            //using list array to connect to the clas project and defining commandtype as stored procedure and assing to the list
            List<project> list = con.Query<project>("ViewOrSearch", param, commandType: CommandType.StoredProcedure).ToList<project>();

            //assing the list to the data grid view
            dgvproject.DataSource = list;
            dgvproject.Columns[0].Visible = false;
        }

        /// <summary>
        /// this method is called when the search button is clicked
        /// it enables to fill data according the text value
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            fillData();
        }

        /// <summary>
        /// this method is sed to populate data in a combobox by retrieving data from the database
        /// </summary>
        void fillcombo()
        {
            //creating connection string
            String str = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;";

            //creating query string for sql
            String sql = "select * from Users where role = 'Developer' ";

            //connecting to the database
            SqlConnection con = new SqlConnection(str);

            //preparing the query
            SqlCommand cmd = new SqlCommand(sql, con);

            //data readeer to read the data
            SqlDataReader myReader;
            try
            {
                //opening connection and executing the reader
                con.Open();
                myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    //creating variable to store the reader value
                    String dev = myReader.GetString(3);

                    //adding to the combo box
                    cmbDev.Items.Add(dev);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// this method is sed to populate data in a combobox by retrieving data from the database
        /// </summary>
        void fillcomboTest()
        {
            String str = @"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;";
            String sql = "select * from Users where role = 'Tester' ";
            SqlConnection con = new SqlConnection(str);
            SqlCommand cmd = new SqlCommand(sql, con);
            SqlDataReader myReader;
            try
            {
                con.Open();
                myReader = cmd.ExecuteReader();
                while (myReader.Read())
                {
                    String dev = myReader.GetString(3);
                    cmbTest.Items.Add(dev);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }




        /// <summary>
        /// this method clear all the text vale in textboxes
        /// </summary>
        void clear()
        {
            txtDesc.Text = txtpTitle.Text = cmbTest.Text = cmbPrior.Text = cmbDev.Text = "";
            Id = 0;
            Create.Text = "Create";
        }

        /// <summary>
        /// this method i called when button create is clicked
        /// this method enables to insert data into databse table or update it according to the value in the database
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Create_Click_1(object sender, EventArgs e)
        {
            //connecting to the sql database and opening it
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //string variable to store the sql query
            String str = "Select title From projects where title = '" + txtpTitle.Text + "'  ";

            //preparing the query
            SqlCommand cmd1 = new SqlCommand(str, con);

            //fetching the data and storing it to the datatable
            SqlDataAdapter sda = new SqlDataAdapter(str, con);
            System.Data.DataTable dt = new System.Data.DataTable();

            //filling the datatable
            sda.Fill(dt);
            if (dt.Rows.Count > 0)
            {

                MessageBox.Show("Project already exists");
                clear();

            }
            else
            {

                //preparing the query and defining command type as stored procedure
                SqlCommand cmd = new SqlCommand("ProjectAddOrEdit", con);
                cmd.CommandType = CommandType.StoredProcedure;

                //adding parameters to the stored procedure
                cmd.Parameters.AddWithValue("@Id", Id);
                cmd.Parameters.AddWithValue("@title", txtpTitle.Text.Trim());
                cmd.Parameters.AddWithValue("@description", txtDesc.Text.Trim());
                cmd.Parameters.AddWithValue("@start_date", startDate.Text);
                cmd.Parameters.AddWithValue("@end_date", endDate.Text);
                cmd.Parameters.AddWithValue("@priority", cmbPrior.Text.Trim());
                cmd.Parameters.AddWithValue("@developer", cmbDev.Text.Trim());
                cmd.Parameters.AddWithValue("@tester", cmbTest.Text.Trim());
                cmd.Parameters.AddWithValue("@manager", txtManager.Text.Trim());
                cmd.Parameters.AddWithValue("@status", "started");

                //executing the query
                cmd.ExecuteNonQuery();
                if (Id == 0)
                {
                    MessageBox.Show("Project Creation Successfull !!");
                }
                else
                {
                    MessageBox.Show("Project Updated Successfully !!");
                }
                clear();
                fillData();
            }
        }

        /// <summary>
        /// this method will clear all the text in the labels.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnclear_Click(object sender, EventArgs e)
        {
            clear();
        }

        /// <summary>
        /// this method enables to open adding project lablel and close other controls.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addProjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //clearing the text values and closing all the controls
            clear();
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            panel1.Visible = true;
            dgvBugs.Visible = false;
            textSearch.Visible = false;
            butSearch.Visible = false;
            btn_Load.Visible = false;
            dateFrom.Visible = false;
            dateTo.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            btn_Reset.Visible = false;
            btn_Export.Visible = false;
            btn_bugs.Visible = false;

        }

        /// <summary>
        /// this method will enable to view project data in datagridview and close all other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewProjectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //closing all the controls of the form
            dgvproject.Visible = true;
            txtSearch.Visible = true;
            btnSearch.Visible = true;
            panel1.Visible = false;
            dgvBugs.Visible = false;
            textSearch.Visible = false;
            butSearch.Visible = false;
            btn_Load.Visible = false;
            dateFrom.Visible = false;
            dateTo.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            btn_Reset.Visible = false;
            btn_Export.Visible = false;
            btn_bugs.Visible = false;
            fillData();
        }


        /// <summary>
        /// this method enables to delete data in the database using stored procedure and dapper
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_delete_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Are You Sure?", "Project", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                //connecting to the sql database and opening it
                SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
                con.Open();

                //creating dynaimc parameters object 
                DynamicParameters dp = new DynamicParameters();

                //adding parameter and executing the command
                dp.Add("@Id", Id);

                //defining command type as stored procedure
                con.Execute("DeleteById", dp, commandType: CommandType.StoredProcedure);
                clear();
                fillData();
                MessageBox.Show("Deleted Succesfully");

            }
        }


        /// <summary>
        /// this method enables to provide all values of datagrid to text when datagrid is double clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvproject_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            panel1.Visible = true;
            if (dgvproject.CurrentRow.Index != -1)
            {
                //assigning the data grid value to the text boxes
                Id = Convert.ToInt32(dgvproject.CurrentRow.Cells[0].Value.ToString());
                txtpTitle.Text = dgvproject.CurrentRow.Cells[1].Value.ToString();
                txtDesc.Text = dgvproject.CurrentRow.Cells[2].Value.ToString();
                startDate.Text = dgvproject.CurrentRow.Cells[3].Value.ToString();
                endDate.Text = dgvproject.CurrentRow.Cells[4].Value.ToString();
                cmbPrior.Text = dgvproject.CurrentRow.Cells[5].Value.ToString();
                cmbDev.Text = dgvproject.CurrentRow.Cells[6].Value.ToString();
                cmbTest.Text = dgvproject.CurrentRow.Cells[7].Value.ToString();
                txtManager.Text = dgvproject.CurrentRow.Cells[8].Value.ToString();


                Create.Text = "Update";
                btn_Delete.Visible = true;
            }

        }

        /// <summary>
        /// this method enables to open comments form and close all other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CommentsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            panel1.Visible = false;
            btn_Load.Visible = false;
            btn_Delete.Visible = false;
            dgvBugs.Visible = false;
            textSearch.Visible = false;
            butSearch.Visible = false;
            dateFrom.Visible = false;
            dateTo.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            btn_Reset.Visible = false;
            btn_Export.Visible = false;
            btn_bugs.Visible = false;
            Comment cm = new Comment(fullname);
            cm.MdiParent = this;
            cm.Show();
        }

        /// <summary>
        /// this is method that interacts with database and retrives value using stored procedure and stores in lists
        /// </summary>
        void fillBugs()
        {

            //connecting to the database
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");

            //creating dynamic parameters for dapper
            DynamicParameters param = new DynamicParameters();

            //adding parameters to the query
            param.Add("@search", textSearch.Text.Trim());

            //creating list object and defining command type and assigning to the data grid view
            List<bug> list = con.Query<bug>("ViewOrSearchBug", param, commandType: CommandType.StoredProcedure).ToList<bug>();
            dgvBugs.DataSource = list;
            dgvBugs.Columns[0].Visible = false;

        }

        /// <summary>
        /// this method enables to view bug grid view by closing or hiding all other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            panel1.Visible = false;
            btn_Load.Visible = false;
            btn_Delete.Visible = false;
            dgvBugs.Visible = true;
            textSearch.Visible = true;
            butSearch.Visible = true;
            dateFrom.Visible = false;
            dateTo.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            btn_Reset.Visible = false;
            btn_Export.Visible = false;
            btn_bugs.Visible = false;
            fillBugs();
        }

        /// <summary>
        /// this method is used to fill the bugs according to the value provided in search text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSearch_Click(object sender, EventArgs e)
        {
            fillBugs();
        }


        /// <summary>
        /// this method is used in closing this form and opening login form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Hide();
            frmLogin log = new frmLogin();
            log.Show();
        }

        /// <summary>
        /// this meethod is used to close this form 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void exitToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// this method is used for showing progile form and hiding or closing other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //hiding and modifying controls in this form
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            panel1.Visible = false;
            btn_Load.Visible = false;
            btn_Delete.Visible = false;
            dgvBugs.Visible = false;
            textSearch.Visible = false;
            butSearch.Visible = false;
            dateFrom.Visible = false;
            dateTo.Visible = false;
            label10.Visible = false;
            label11.Visible = false;
            btn_Reset.Visible = false;
            btn_Export.Visible = false;
            btn_bugs.Visible = false;

            //creating object of profile form
            Profile p = new Profile(fullname);

            //assigning profile as a child of this form and showing the form
            p.MdiParent = this;
            p.Show();
        }

        /// <summary>
        /// this is the method to show the grid view of bugs and hide or close other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void bugReport_Click(object sender, EventArgs e)
        {

            //hiding and modifying controls in this form
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            panel1.Visible = false;
            btn_Load.Visible = true;
            dgvBugs.Visible = true;
            textSearch.Visible = false;
            butSearch.Visible = false;
            btn_Delete.Visible = false;
            dateFrom.Visible = true;
            dateTo.Visible = true;
            label10.Visible = true;
            label11.Visible = true;
            btn_Reset.Visible = true;
            btn_Export.Visible = false;
            btn_bugs.Visible = true;
            fillBugs();
        }


        /// <summary>
        /// this is the method to show the grid view of projects and hide or close other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void projectReport_Click(object sender, EventArgs e)
        {
            //hiding and modifying controls in this form
            dgvproject.Enabled = false;
            dgvproject.Visible = true;
            dgvproject.Size = new System.Drawing.Size(1200, 529);
            dgvproject.Location = new System.Drawing.Point(18, 103);
            panel1.Visible = false;
            dgvBugs.Visible = false;
            textSearch.Visible = false;
            butSearch.Visible = false;
            btn_Load.Visible = true;
            btn_Delete.Visible = false;
            dateFrom.Visible = true;
            dateTo.Visible = true;
            label10.Visible = true;
            label11.Visible = true;
            btn_Reset.Visible = true;
            btn_Export.Visible = true;
            btn_bugs.Visible = false;
            fillData();

        }
        /// <summary>
        /// this is the method which connects with the database and executes query to fill the grid view for bugs
        /// </summary>
        void loadBugs()
        {
            //connecting to the database
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");

            //assigning query script to the string variable
            string query = "SELECT  p.title,p.developer,p.tester,p.manager,b.* FROM Bugs b,Projects p where b.start_time between '" + dateFrom.Value + "' and '" + dateTo.Value + "' ";

            //creating list object and defining command type and assigning to the data grid view
            List<bug> list = con.Query<bug>(query, commandType: CommandType.Text).ToList<bug>();
            dgvBugs.DataSource = list;
        }


        /// <summary>
        /// this is the method which connects with the database and executes query to fill the grid view for projects
        /// </summary>
        void loadProjects()
        {
            //connecting to the database
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");

            //assigning query script to the string variable
            string query = "SELECT * FROM Projects  where start_date between '" + dateFrom.Value + "' and '" + dateTo.Value + "' ";

            //creating list object and defining command type and assigning to the data grid view
            List<project> list = con.Query<project>(query, commandType: CommandType.Text).ToList<project>();
            dgvproject.DataSource = list;
        }

        /// <summary>
        /// this method will help to fill datagridview according to the selected date
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Load_Click(object sender, EventArgs e)
        {
            loadBugs();
            loadProjects();
        }

        /// <summary>
        /// this method will fill the datagridview of both bugs and project.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Reset_Click(object sender, EventArgs e)
        {
            fillBugs();
            fillData();
        }


        /// <summary>
        /// this is the methods that uses export code and add excel cell values to the datagrid view columns and rows
        /// </summary>
        /// <param name="dgv"></param>
        void toExcel(DataGridView dgv)
        {

            //calling the object of microsoft ecel application
            Microsoft.Office.Interop.Excel.Application app = new Microsoft.Office.Interop.Excel.Application();

            //setting app interactive to false
            app.Interactive = false;

            //creating worbook object and worksheet object of microsoft excel
            Microsoft.Office.Interop.Excel.Workbook wb = app.Workbooks.Add(Microsoft.Office.Interop.Excel.XlWBATemplate.xlWBATWorksheet);
            Microsoft.Office.Interop.Excel.Worksheet ws = (Microsoft.Office.Interop.Excel.Worksheet)wb.ActiveSheet;

            //assigning header text to the excel cells
            for (int i = 0; i < dgv.Columns.Count; i++)
            {
                ws.Cells[1, i + 1] = dgv.Columns[i].HeaderText;
            }

            //assigning datagrid view rows to excel rows
            for (int i = 0; i < dgv.Rows.Count; i++)
            {
                for (int j = 0; j < dgv.Columns.Count; j++)
                {
                    ws.Cells[i + 2, j + 1] = Convert.ToString(dgv.Rows[i].Cells[j].Value.ToString());

                }

            }

            //cearting object of save file
            SaveFileDialog saveFileDialog = new SaveFileDialog();

            //filter the extension types
            saveFileDialog.Filter = "Excel WorkBook (*.xlsx)|*.xlsx|All Files (*.*)|*.*";
            saveFileDialog.ValidateNames = true;
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                //saving workbook and closing it
                wb.SaveAs(saveFileDialog.FileName, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Microsoft.Office.Interop.Excel.XlSaveAsAccessMode.xlExclusive, Type.Missing, Type.Missing, Type.Missing, Type.Missing, Type.Missing);
                wb.Close();
            }

            //quiting the app
            app.Quit();
        }

        /// <summary>
        /// this method will help in ecporting project datagridview into excel format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_Export_Click(object sender, EventArgs e)
        {
            //calling method toExcel
            toExcel(dgvproject);

        }

        /// <summary>
        /// this method will help in exporting the datagridview Bugs to excel format
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btn_bugs_Click(object sender, EventArgs e)
        {
            toExcel(dgvBugs);
        }
    }
}

    /// <summary>
    /// creating a class name project and adding get set method for dapper extension.
    /// </summary>
    class project
    {
        public int Id { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string start_date { get; set; }
        public string end_date { get; set; }
        public string priority { get; set; }
        public string developer { get; set; }
        public string tester { get; set; }
        public string manager { get; set; }
        public string status { get; set; }
    }

