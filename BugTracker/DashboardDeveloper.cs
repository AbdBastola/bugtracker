﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
using System.Diagnostics;
using Dapper;
using System.IO;

namespace BugTracker
{

    /// <summary>
    /// this class contains the methods used in this form
    /// </summary>
    public partial class DashboardDeveloper : Form
    {
        private string FullPathName = "";

        private string fullname = "";
        int Id = 0;
        int Ide = 0;

        /// <summary>
        /// this is the constructor of the class dashboard
        /// </summary>
        /// <param name="role"></param>
        /// <param name="name"></param>
        public DashboardDeveloper(String role, String name)
        {
            //initlializing the components
            InitializeComponent();
            Status.Text = role;
            label1.Text = "Welcome!!" + name;
            
             tby.Text = name;
            fullname = name;
            editProfileToolStripMenuItem.Text = name;
            
            //checking the roles
            if(role == "Developer")
            {
                this.cmbStatus.Items.AddRange(new object[] {
            "Started",
            "Developing",
            "Finished"
           });
                addBugs.Visible = false;
               
            }
            else if(role == "Tester")
            {
                this.cmbStatus.Items.AddRange(new object[] {
            "Testing"
            
           });
                
                
                dgvBugs.Enabled = true;
               
            }
            
        }

       
      /// <summary>
      /// this method is called when open mwnu is clicked
      /// this method enables to open files and read it to the application
      /// </summary>
      /// <param name="sender"></param>
      /// <param name="e"></param>
        private async void OpenFile(object sender, EventArgs e)
        {
            
            addBugs.Enabled = true;
            
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            openFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            if (openFileDialog.ShowDialog(this) == DialogResult.OK)
            { 
                FullPathName = openFileDialog.FileName.ToString();
                txtRead.Visible = true;
                numberLabel.Visible = true;
                StreamReader sr = new StreamReader(openFileDialog.FileName);
                txtRead.Text = await sr.ReadToEndAsync();
                sr.Close();
                saveToolStripButton.Enabled = true;
                saveAsToolStripMenuItem.Enabled = true;
                saveToolStripMenuItem.Enabled = true;
                dgvBugs.Visible = false;
                textSearch.Visible = false;
                butSearch.Visible = false;
                txtRead.Size = new System.Drawing.Size(1275, 630);
                panelbug.Visible = false;
                dgvproject.Visible = false;
                txtSearch.Visible = false;
                btnSearch.Visible = false;
                label8.Visible = false;
                comboStat.Visible = false;
                butUpdate.Visible = false;


            }
        }

        /// <summary>
        /// this method is called when saveas menu is clickeds
        /// this method enables to save the file and write it
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SaveAsToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            txtRead.SelectionColor = Color.Red;
            txtRead.DeselectAll();

            //opening the save dialog form and filtering the form extension
            SaveFileDialog saveFileDialog = new SaveFileDialog();
            saveFileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.Personal);
            saveFileDialog.Filter = "Text Files (*.txt)|*.txt|All Files (*.*)|*.*";
            saveFileDialog.ValidateNames = true;
            if (saveFileDialog.ShowDialog(this) == DialogResult.OK)
            {
                FullPathName = saveFileDialog.FileName.ToString();

                //writing the file using streamwriter
                StreamWriter sw = new StreamWriter(saveFileDialog.FileName);
                 sw.WriteLine(txtRead.Text);
           
                //closing the stram writer
                sw.Close();
                MessageBox.Show("You have sucessfully saved the file ", "Message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                saveToolStripButton.Enabled = true;
                saveToolStripMenuItem.Enabled = true;


            }
        }

        /// <summary>
        /// this method is called when exit menu is clicked
        /// this method enables to exit the current form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// this method will help to interact with toolbar menu in dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ToolBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            toolStrip.Visible = toolBarToolStripMenuItem.Checked;
        }

        /// <summary>
        /// this method will help to interact with status bar menu in dashboard
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void StatusBarToolStripMenuItem_Click(object sender, EventArgs e)
        {
            statusStrip.Visible = statusBarToolStripMenuItem.Checked;
        }

        /// <summary>
        /// tthis method is called when projects menu is clicked
        /// this method will open project grid view and close all the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void projectsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label2.Visible = false;
            cmbStatus.Visible = false;
            btnUpdate.Visible = false;
            dgvproject.Visible = true;
            txtSearch.Visible = true;
            btnSearch.Visible = true;
            txtRead.Visible = false;
            numberLabel.Visible = false;
            panelbug.Visible = false;
            addBugs.Enabled = false;
       
            dgvBugs.Visible = false;
            textSearch.Visible = false;
            butSearch.Visible = false;
            label8.Visible = false;
            comboStat.Visible = false;
            butUpdate.Visible = false;

        }

        /// <summary>
        /// this method connects to the database and fetch data using dapper and stored procedure
        /// </summary>
        void fillData()
        {

            //connecting to the database
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");

            // creating dynamic parameters for dapper
             DynamicParameters param = new DynamicParameters();

            //adding parameters to the query
            param.Add("@SearchText", txtSearch.Text.Trim());

            //creating list object and defining command type and assigning to the data grid view
            List<project> list = con.Query<project>("ViewOrSearch", param, commandType: CommandType.StoredProcedure).ToList<project>();
            dgvproject.DataSource = list;
            dgvproject.Columns[0].Visible = false;
        }

        /// <summary>
        /// this method connects to the database and fetch data using dapper and stored procedure
        /// </summary>
        void fillBugs()
        {
            //connecting to the database
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");

            // creating dynamic parameters for dapper
            DynamicParameters param = new DynamicParameters();

            //adding parameters to the query
            param.Add("@search", textSearch.Text.Trim());

            //creating list object and defining command type and assigning to the data grid view
            List<bug> list = con.Query<bug>("ViewOrSearchBug", param, commandType: CommandType.StoredProcedure).ToList<bug>();
            dgvBugs.DataSource = list;
            dgvBugs.Columns[0].Visible = false;
           
            

        }

        /// <summary>
        /// this method is called when the form is loaded
        /// this method enables to load all the method and control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DashboardDeveloper_Load(object sender, EventArgs e)
        {
            // TODO: This line of code loads data into the 'bugsDataSet.Projects' table. You can move, or remove it, as needed.
            this.projectsTableAdapter.Fill(this.bugsDataSet.Projects);
            label2.Visible = false;
            cmbStatus.Visible = false;
            btnUpdate.Visible = false;

            //this code helps in connecting to the database and open the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //this code stores the sql query as string
            String str = "Select priority From Bugs";

            //this codes prepares the sql statements
            SqlCommand cmd1 = new SqlCommand(str, con);

            //this code makes an object of data table and loads the data to the table
            DataTable dt = new DataTable();
            dt.Load(cmd1.ExecuteReader());
            foreach (DataRow row in dt.Rows)
            {
                
                    if (row["priority"].ToString() == "High")
                    {
                        dgvBugs.Columns[6].DefaultCellStyle.BackColor = Color.Red;
                    }

                
            }


        
            txtRead.Visible = false;
            fillData();
            fillBugs();
            

        }
       
        /// <summary>
        /// this method is called when the search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnSearch_Click(object sender, EventArgs e)
        {
            fillData();
        }


        /// <summary>
        /// this method is called when the cell of grid view is double clicked
        /// this method enables to load the values of cells in the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvproject_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            label2.Visible = true;
            cmbStatus.Visible = true;
            btnUpdate.Visible = true;

            //this code checks whether the row index of datagrid view is less than one or not
            if (dgvproject.CurrentRow.Index != -1)
            {

                //this code assingss the rows cells value to texts and variables
                Id = Convert.ToInt32(dgvproject.CurrentRow.Cells[0].Value.ToString());
                cmbStatus.Text = dgvproject.CurrentRow.Cells[9].Value.ToString();

                
            }
        }

        /// <summary>
        /// this method is called when update button is clicked
        /// this button enables to update data in the database using stored procedure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            //this code connects to the databse and opens the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //this code prepare the sql statement and defines the command type as stored procedure
            SqlCommand cmd = new SqlCommand("StatusUpdate", con);
            cmd.CommandType = CommandType.StoredProcedure;

            //this code adds parameter value to the stored procedure
            cmd.Parameters.AddWithValue("@Id", Id);
            cmd.Parameters.AddWithValue("@status", cmbStatus.Text.Trim());

            //this code executes the statement
            cmd.ExecuteNonQuery();
            MessageBox.Show("Project Updated Successfully !!");
            fillData();
        }

        /// <summary>
        /// this method helps to provide the number in the text box
        ///
        /// </summary>
        private void updateNumberLabel()
        {
            //we get index of first visible char and number of first visible line
            Point pos = new Point(0, 0);
            int firstIndex = txtRead.GetCharIndexFromPosition(pos);
            int firstLine = txtRead.GetLineFromCharIndex(firstIndex);

            //now we get index of last visible char and number of last visible line
            pos.X = ClientRectangle.Width;
            pos.Y = ClientRectangle.Height;
            int lastIndex = txtRead.GetCharIndexFromPosition(pos);
            int lastLine = txtRead.GetLineFromCharIndex(lastIndex);

            //this is point position of last visible char, we'll use its Y value for calculating numberLabel size
            pos = txtRead.GetPositionFromCharIndex(lastIndex);


            //finally, renumber label
            numberLabel.Text = "";
            for (int i = firstLine; i <= lastLine + 1; i++)
            {
                numberLabel.Text += i + 1 + "\n";
            }

        }

        /// <summary>
        /// this method is called when the text of the text box is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void richTextBox1_TextChanged(object sender, EventArgs e)
        {
            updateNumberLabel();
        }

        /// <summary>
        /// this method is called when the text box is scrolled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void richTextBox1_VScroll(object sender, EventArgs e)
        {
            numberLabel.Text = "";

            numberLabel.Invalidate();
            updateNumberLabel();
        }


        /// <summary>
        /// this method is called when the text box is resized
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void richTextBox1_Resize(object sender, EventArgs e)
        {
            richTextBox1_VScroll(null, null);
        }

        /// <summary>
        /// this method is called when the text box font is changed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void richTextBox1_FontChanged(object sender, EventArgs e)
        {
            updateNumberLabel();
            richTextBox1_VScroll(null, null);
        }


        /// <summary>
        /// this method is called when save button is clicked
        /// this method enables to save the text
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void saveToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //assigning a variable for text value
            string newContent = txtRead.Text;

            //writing the file to its location
            StreamWriter sw = new StreamWriter(FullPathName);
            sw.Write(newContent);

            //closing the streamwriter
            sw.Close();
            MessageBox.Show("File Saved");
        }

     

       
        /// <summary>
        /// this method is called when add bugs button is clicked
        /// this method open the label for adding bugs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void addBugs_Click(object sender, EventArgs e)
        {
            txtRead.Size = new System.Drawing.Size(750, 630);
            panelbug.Visible = true;
        }

        /// <summary>
        /// this method is called when the bug is added
        /// this method enables to add data into database using stored procedure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAddbug_Click(object sender, EventArgs e)
        {

            //connecting to the database and opening the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();


            //preparing and declaring the command type as stored procedure
            SqlCommand cmd = new SqlCommand("InsertBug", con);
            cmd.CommandType = CommandType.StoredProcedure;

            //providing parameter value from the texts
            cmd.Parameters.AddWithValue("@Id", SqlDbType.Int).Direction = ParameterDirection.Output;
            cmd.Parameters.AddWithValue("@project_title",Convert.ToInt32( cmbTitle.SelectedValue));
            cmd.Parameters.AddWithValue("@bug_title", txtBug.Text.Trim());
            cmd.Parameters.AddWithValue("@status", "started");
            cmd.Parameters.AddWithValue("@filename", txtFile.Text.Trim());
            cmd.Parameters.AddWithValue("@classname", txtClass.Text.Trim());
            cmd.Parameters.AddWithValue("@methodname", txtMethod.Text.Trim());
            cmd.Parameters.AddWithValue("@linenumber",Convert.ToInt32( txtLine.Text.Trim()));
            cmd.Parameters.AddWithValue("@link", txtLink.Text.Trim());
            cmd.Parameters.AddWithValue("@start_time", DateTime.Now.ToString("MM/dd/yyyy"));
            cmd.Parameters.AddWithValue("@priority", cmbPriority.Text.Trim());

            //executing the statement
            cmd.ExecuteNonQuery();


            //getting the current inserted ID
            string lastid = cmd.Parameters["@Id"].Value.ToString();

            //preparing and declaring the command type as stored procedure
            SqlCommand cmd1 = new SqlCommand("InsertComments", con);
            cmd1.CommandType = CommandType.StoredProcedure;

            //adding parameters value from the text
            cmd1.Parameters.AddWithValue("@comments", bugcoment.Text.Trim());
            cmd1.Parameters.AddWithValue("@bugs_id", lastid);
            cmd1.Parameters.AddWithValue("@posted_by", tby.Text.Trim());

            //executing the statement
            cmd1.ExecuteNonQuery();
            MessageBox.Show("Bug Posted !!");
            clear();
            fillBugs();
            
        }

        /// <summary>
        /// this method clears the text values
        /// </summary>
        void clear()
        {
            txtBug.Text =txtLine.Text = txtLink.Text = txtFile.Text = txtMethod.Text = txtClass.Text = bugcoment.Text = "";
        }


        /// <summary>
        /// this method is called when view buhs menu is clicked
        /// this method enables to open bugs grid view and close all the other controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void viewBugsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            dgvBugs.Visible = true;
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            txtRead.Visible = false;
            numberLabel.Visible = false;
            panelbug.Visible = false;
            addBugs.Enabled = false;
            
            textSearch.Visible = true;
            butSearch.Visible = true;
            label2.Visible = false;
            cmbStatus.Visible = false;
            btnUpdate.Visible = false;
            label8.Visible = false;
            comboStat.Visible = false;
            butUpdate.Visible = false;
        }


        /// <summary>
        /// this method is called when search button is clicked
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butSearch_Click(object sender, EventArgs e)
        {
            fillBugs();
        }


        /// <summary>
        /// this method is called when the cell of grid view is double clicked
        /// this method enables to load the values of cells in the controls
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dgvBugs_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            label8.Visible = true;
            comboStat.Visible = true;
           butUpdate.Visible = true;

            //checking whether the row index is less than one or not
            if (dgvBugs.CurrentRow.Index != -1)
            {

                //assinging the data grid cells value to text and variable
                Ide = Convert.ToInt32(dgvBugs.CurrentRow.Cells[0].Value.ToString());
                comboStat.Text = dgvBugs.CurrentRow.Cells[6].Value.ToString();
                

            }
        }


        /// <summary>
        /// this method is called when comment menu is clicked
        /// this method open the comment form all closes all the control
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void commentToolStripMenuItem_Click(object sender, EventArgs e)
        {
            label2.Visible = false;
            cmbStatus.Visible = false;
            btnUpdate.Visible = false;
            dgvproject.Visible = false;
            txtSearch.Visible = false;
            btnSearch.Visible = false;
            txtRead.Visible = false;
            numberLabel.Visible = false;
            panelbug.Visible = false;
            addBugs.Enabled = false;
            
            dgvBugs.Visible = false;
            textSearch.Visible = false;
            butSearch.Visible = false;
            label8.Visible = false;
            comboStat.Visible = false;
            butUpdate.Visible = false;

            //opening the comment form
            Comment cm = new Comment(fullname);
            cm.MdiParent = this;
            cm.Show();


        }

        /// <summary>
        /// this method is called when update button is clicked
        /// this method enables to update the database table using stored procedure
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void butUpdate_Click(object sender, EventArgs e)
        {

            //connection to the database and open the connection
            SqlConnection con = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\Abd\source\repos\BugTracker\Database\bugs.mdf;Integrated Security=True;Connect Timeout=30;");
            con.Open();

            //defining sql command type as stored procedure and preparing it
            SqlCommand cmd = new SqlCommand("BugsUpdate", con);
            cmd.CommandType = CommandType.StoredProcedure;

            //providing parameter value to the stored procedure
            cmd.Parameters.AddWithValue("@Id", Ide);
            cmd.Parameters.AddWithValue("@status", comboStat.Text.Trim());

            //executing the query
            cmd.ExecuteNonQuery();
           
            //checking whether the combox box value is finished or not
            if(comboStat.Text == "Finished")
            {
                //writing the query and storing it to string
                String str = " Update Bugs set end_time = @end_time where Id = '"+Ide+"' ";

                //preparing the query
                SqlCommand cmd1 = new SqlCommand(str, con);

                //passing parameters to the query
                cmd1.Parameters.Add(new SqlParameter("@end_time", DateTime.Now.ToString("MM/dd/yyyy")));

                //executing and closing the statement
                cmd1.ExecuteNonQuery();
                con.Close();
            }
            MessageBox.Show("Bug Status Updated Successfully !!");
            fillBugs();

        }


        /// <summary>
        /// this method is called when logout menu is clicked
        /// this button enables to close the current form and oopen the logout form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void logoutToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            this.Close();
            frmLogin fm = new frmLogin();
            fm.Show();
        }

        /// <summary>
        /// this method is called when edit menu is clicked
        ///this method enables to open the profile form and clode all the other forms
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void editProfileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profile p = new Profile(fullname);
            p.MdiParent = this;
            p.Show();
        }
    }
}


/// <summary>
/// this class contains gettter and setter method which is used in dapper
/// </summary>
class bug
{
    public int Id { get; set; }
    public string bug_title { get; set; }
    public string title { get; set; }
    public string developer { get; set; }
    public string tester { get; set; }
    public string manager { get; set; }
    public string status { get; set; }
    public string filename { get; set; }
    public string classname { get; set; }
    public string methodname { get; set; }
    public string linenumber { get; set; }
    public string link { get; set; }
    public string start_time { get; set; }
    public string end_time { get; set; }
    public string priority { get; set; }


}
